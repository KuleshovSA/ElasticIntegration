﻿
Процедура ЗагрузитьСтруктуруНаСервере(Объект) Экспорт
	
	Объект.Настройки.Очистить();
	
	СтруктураИндекса = ЛогированиеНаСервере.ПолучитьСтруктуруИндекса(Объект.Ссылка);
	
	Если НЕ ЗначениеЗаполнено(СтруктураИндекса) Тогда
		
		Сообщить("Индекс не создан! Для инициализации индекса необходимо выгрузить данные");
		Возврат;
		
	КонецЕсли;
	
	Для Каждого КлючИЗначение Из СтруктураИндекса Цикл
		
		Маппинг = КлючИЗначение.Значение["mappings"];
		Прервать;
		
	КонецЦикла;
	
	Если Маппинг.Получить("properties") = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	Для Каждого Колонка Из Маппинг["properties"] Цикл
		
		ИмяКолонки = Колонка.Ключ;
		
		Если НЕ Колонка.Значение.Получить("type") = Неопределено Тогда
			
			ТипЭластик = Колонка.Значение["type"];
			
			Если ТипЭластик = "text" Тогда
				ОписаниеТипа = Новый ОписаниеТипов("Строка");
			ИначеЕсли ТипЭластик = "date" Тогда
				ОписаниеТипа = Новый ОписаниеТипов("Дата");
			ИначеЕсли ТипЭластик = "boolean" Тогда
				ОписаниеТипа = Новый ОписаниеТипов("Булево");
			ИначеЕсли ТипЭластик = "long"
				ИЛИ ТипЭластик = "integer"
				ИЛИ ТипЭластик = "short"
				ИЛИ ТипЭластик = "byte"
				ИЛИ ТипЭластик = "double"
				ИЛИ ТипЭластик = "float"
				ИЛИ ТипЭластик = "half_float"
				ИЛИ ТипЭластик = "scaled_float" Тогда
				
				ОписаниеТипа = Новый ОписаниеТипов("Число");
			Иначе
				ОписаниеТипа = Новый ОписаниеТипов("Строка");
			КонецЕсли;
			
			СтрокаНастроек = Объект.Настройки.Добавить();
		
			СтрокаНастроек.Колонка 			= ИмяКолонки;
			СтрокаНастроек.ТипДанныхЭластик = ТипЭластик;
			СтрокаНастроек.ТипДанных1С		= Строка(ОписаниеТипа);
			
		КонецЕсли;
		
	КонецЦикла;
	
КонецПроцедуры